import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './components/forms/login-form/login-form.component';
// import { DashboardComponent } from './components/forms/dashboard/dashboard.component';
import { RegisterFormComponent } from './components/forms/register-form/register-form.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./components/forms/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'register',
    component: RegisterFormComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'surveys',
    loadChildren: () => import('./components/forms/survey-list/survey-list.module').then(m => m.SurveyListModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'surveys/:id',
    loadChildren: () => import('./components/forms/survey-detail/survey-detail.module').then(m => m.SurveyDetailModule),
    canActivate: [AuthGuard]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
