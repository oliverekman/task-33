import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})



export class AuthService {

  
  constructor(private http: HttpClient) { }

  // uasername, password
  register(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      user: {...user}
    }).toPromise()
  }
  

  login(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login`, {
      user: {...user}
    }).toPromise()
  }

    // private user: any = null;


  /**
   * Attempt to login with given user
   * @param user 
   * @returns boolean
   */
  // public login(user: any): boolean {

  //   if (user.username == 'Oliver' && user.password == 'secret') {
  //     this.user = {...user};

  //   } else {
  //     this.user = null;
  //   }

  //   return this.isLoggedIn();
  // }

  /**
   * Check if a user is currently logged in
   * @returns boolean
   */
  // public isLoggedIn(): boolean {
  //   return this.user !== null;
  // }

}
