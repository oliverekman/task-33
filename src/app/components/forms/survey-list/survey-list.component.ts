import { Component, OnInit } from '@angular/core';
import { SurveyServiceService } from 'src/app/services/SurveyService/survey-service.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  surveys: any[] = [];
  surveyListError: string;


  constructor(private surveyService: SurveyServiceService, private router: Router) { }

  async ngOnInit() {
    try {

      this.surveys = await this.surveyService.getSurveys();
      
    } catch (e) {

      this.surveyListError = e.message || e;

    }
  }

  onSurveyClicked(surveyId: number) {
    this.router.navigate(['/surveys', surveyId]);
  }


}
