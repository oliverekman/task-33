import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router'
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private session: SessionService) { }

  ngOnInit(): void {
  }


  get username() {
    return this.session.get().username
  }

  // get isLoggedIn() {
  //   return this.authService.isLoggedIn()
  // }

  logoutNowClick() {
    this.router.navigateByUrl('/login')
  }

}
