import { Component, Input, Output,  EventEmitter } from '@angular/core';
import { Xtb } from '@angular/compiler';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent  {
  
  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.email]),
    password: new FormControl('', [ Validators.required, Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{4,}$')]),
    confirmPassword: new FormControl('', [ Validators.required, Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{4,}$')])
  })

  isLoading: Boolean = false;
  loginError: string;



  constructor(private userService: AuthService, private router: Router, private session: SessionService) { 

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }
    
  }

 

  get username() {
    return this.registerForm.get('username')
  }
  get password() {
    return this.registerForm.get('password')
  }
  get confirmPassword() {
    return this.registerForm.get('confirmPassword')
  }
  

  async onRegisterClick() {

    try {

      this.isLoading = true;
      if (this.password.value !== this.confirmPassword.value) {
        return alert('You have to write the same password smart as...')
      }
      const result: any = await this.userService.register(this.registerForm.value)

      if (result.status < 400) {
        this.session.save({
          token: result.data.token,
          username: result.data.user.username
      });
        this.router.navigateByUrl('/dashboard')
      }

    } catch (e) {
      console.error(e);
    } finally {
      this.isLoading = false;
    }

  }



}
