import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.email]),
    password: new FormControl('', [ Validators.required, Validators.minLength(6), Validators.maxLength(20)])
  });

  isLoading: boolean = false;
  loginError: string;


  constructor(private authService: AuthService, private router: Router, private session: SessionService) {

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }

   }

 

  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }



  async onLoginClick() {

    this.loginError = '';

    try {
      this.isLoading = true;
      const result: any = await this.authService.login( this.loginForm.value );

      if (result.status < 400 ){
        this.session.save({ token: result.data.token, username: result.data.user.username});
        this.router.navigateByUrl('/dashboard');
      }

    } catch (e) {

      this.loginError = e.error.error;

    } finally {
      this.isLoading = false;
    }



  }

  registerNowClick() {
    this.router.navigateByUrl('/register')
  }


}
